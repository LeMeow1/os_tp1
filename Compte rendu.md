# TP1 systemd :

## I. systemd-basics
### 1. First steps :

Via la commande top il est possible d'avoir la liste de processus et leur utilisation ressources en temps réel. On peut constater que systemd est bien le PID 1 :

![1](TheForbiddenFolder/1.png)

Via `ps -ef` on retrouve la liste des processus en cours :
polkitd permet de gérer les droits utilisateurs
systemd permet de gérer tout les autres process
NetworkManager permet de gérer les interfaces réseau et leur configuration
sshd permet de gérer le serveur ssh local
auditd permet de gérer le userspace
ModemManager contrôle le wifi et le bluetooth des cartes réseau
firewalld est le firewall fedora
mcelog monitor le hardware et remonte les erreurs
rngd permet de vérifier si les données censées être random le sont suffisamment ou non
abrtd surveille les crash d’applications et vient collecter les données liées au crash
rsyslogd log les évenements du système UNIX
smartd monitor l’auto-monitoring des disques durs
chronyd permet d’ajuster le taux de l’horloge du système
atd permet de lancer les tâches d’une liste de tâches

 ![2](TheForbiddenFolder/2.png)
 
 
### 2. Gestion du temps

Local Time : L’heure de la zone où l’on se situe, soit en europe, soit dans la zone CET (Central European Time), ces zones sont définies par la latitude relative aux zones géographiques.

Universal Time : C’est le temps défini par les rotations terrestres, UTC est sont amélioration prenant en compte les autres corps célestes afin d’avoir une mesure plus proche du temps solaire.

RTC Time : Real Time Clock, soit l’horloge interne de la machine qui calcul le temps qui passe.

Il est utile d’utiliser le RTC pour connaître la durée exacte d’exécution des processus sur sa machine ainsi que pour les modifications systèmes ou file system afin d’éviter des décalages temporaires qui peuvent créer des erreurs.

Afin de changer de timezone pour utiliser soit une autre zone géographique ou le temps UTC il est possible d’exécuter la commande `timedatectl timezone ZONE`, ce qui donne ceci :

 ![3](TheForbiddenFolder/3.png)
 
Pour vérifier si le serveur NTP est actif ou non sur la machine il faut vérifier le service crhonyd, on utilise `systemctl status chronyd` :

 ![4](TheForbiddenFolder/4.png)
 
 
### 3. Gestion de noms
L’utilitaire hostnamectl permet de gérer les noms de la machine, effectivement la machine possède trois noms différents :

- --Le « pretty name » littéralement le joli nom est un type de nom pouvant contenir des apostrophes et des accents afin d’être… joli
- --Le « static name » qui est l’équivalent du pretty name mais ne contenant pas de caractères spéciaux car il sera utiliser pour l’initialisation du kernel notamment qui n’est pas adepte des accents.
- --Le « transient ou falback name » qui est une valeur de secours utilisée si aucun static name n’est défini.

Le plus simple en production est d’utiliser le static name qui permettra d’insérer les machines dans un serveur DNS par exemple.


### 4. Gestion du réseau

Pour afficher les informations concernant le dhcp via network manager on peut faire un `nmcli con show | grep -i dhcp` :

 ![5](TheForbiddenFolder/5.png)
 
Lors de l’utilisation de networkd on coupe network-manager :

 ![6](TheForbiddenFolder/6.png)
 
Pour éviter que le service network-manager ne redémarre automatiquement il faut vérifier dans `/etc/sysconfig/network-scripts/nom\_int` que les interfaces ne soient pas pilotées via network-manager. Il existe ou non une clef `NM\_CONTROLLED` qu’il faut définir à `no` si elle existe.

On peut ensuite lancer networkd :

 ![7](TheForbiddenFolder/7.png)
 
Le service pleut s’appuyer sur des fichiers de conf `.network` pour les configurations de cartes réseaux, ci-dessous un exemple minimaliste :

 ![8](TheForbiddenFolder/8.png)
 
On passe à resolved qu’on active via `systemctl start systemd-resolved` et qu’on active au démarrage via `systemctl enable system-resolved` et on peut vérifier le lancement du service via `systemctl status systemd-resolved`.

On peut trouver grâce à la commande `ss -l` qui va lister les services locaux le DNS tournant en local :

 ![9](TheForbiddenFolder/9.png)
 
Il est possible d’effectuer des requêtes DNS via dig ou bien directement `systemd-resolve` :

 ![10](TheForbiddenFolder/10.png)
 
 ![11](TheForbiddenFolder/11.png)
 
 ![13](TheForbiddenFolder/13.png)
 
On peut configurer notre DNS à partir du fichiet `/etc/systemd/resolved.conf`, on active ici TLS

 ![14](TheForbiddenFolder/14.png)
 
Après avoir fait ça les requêtes DNS devraient se faire en DNS sur TLS, voici mes trames :

 ![15](TheForbiddenFolder/15.png)
 
 
### 5. Gestion de logind

Permet de gérer les sessions utilisateurs et les utilisateurs.
On peut par exemple afficher la liste de sessions actives avec `loginctl list-sessions` ou encore tuer des sessions avec `loginctl kill-session ID`.


### 6. Gestion d’unité basique

Pour lister toutes les unités : `systemctl`
Uniquement les services : `systemctl -t service`

pour obtenir plus de détails sur une unitée donnée

- systemctl is-active <UNIT>
  - détermine si l’unité est actuellement en cours de fonctionnement
- systemctl is-enabled <UNIT>
  - détermine si l’unité est liée à un target (généralement, on s’en sert pour activer des unités au démarrage)
- systemctl status <UNIT>
  - affiche l’état complet d’une unité donnée
  - comme le path où elle est définier, si elle a été enable, tous les processus liés, etc.
- systemctl cat <UNIT>
  - affiche les fichiers qui définissent l’unité

On peut donc trouver des unités via leur nom s’il est connu ou bien via la PID du service si l’on veut trouver le service du processus inconnu :

 ![16](TheForbiddenFolder/16.png)
 
 
 
## II. Boot et logs

On peut loger les évenements du boot via la commande `systemd-analyze plot > boot.svg` qui s’appuie sur journald. Dans le fichier boot.svg on retrouve tous les services et processus lancés au démarrage de la machine. Avec un grep on peut trouver facilement le service qui nous intéresse, ci-dessous le service sshd qui a mis 286ms a démarré :

 ![17](TheForbiddenFolder/17.png)
 
L'outil de débug qu'on utilise tout le temps pour nos services est journalctl, qui dépend de journald.



## III. Mécanismes manipulés par systemd
### 1. cgroups

Les cgroups sont désormais profondément intégrés aux systèmes GNU/Linux et systemd a été construit avec les cgroups en tête.

- un _scope_ systemd est un cgroup qui contient des processus non-gérés par systemd
- un _slice_ systemd est un cgroup qui contient des processus directement gérés par systemd
- systemd-cgls
  - affiche une structure arborescente des cgroups
- systemd-cgtop
  - affiche un affichage comme top avec l’utilisation des ressources en temps réel, par cgroups
- ps -e -o pid,cmd,cgroup
  - ajoute l’affichage des cgroups à ps
- cat /proc/<PID>/cgroup

On peut changer des paramètres de slices directement via `systemctl set-property PARAMETRE`, l’exemple avec la RAM max du slice utilisateur :

 ![18](TheForbiddenFolder/18.png)
 
Un petit aperçu des autres paramètres modifiables :

 ![19](TheForbiddenFolder/19.png)
 
Chaque modification de paramètre créer un fichier correspondant, il est possible de supprimer le fichier pour retourner à la valeur par défaut :

 ![20](TheForbiddenFolder/20.png)
 
 
### 2. dbus

GUI pour dbus : d-feet

Les commandes pour manipuler dbus sont `dbus-monitor` (écouter les évènements) et `dbus-send` (envoyer des signaux ou appeler des méthodes)

- dbus-monitor --system pour observer le bus système

Si l’on démarre un service comme par exemple firewalld on retrouve une tonne de signaux différents, en voici un défini :

 ![21](TheForbiddenFolder/21.png)
 
Dans ce signal on y retrouve la durée d’envoi du signal l’expéditeur ici 1.77, la destination ici null. Le plus interessant est le path qui renvoi vers `FirewallD1/config`. L’interface est le nom de l’interface dbus qu’on pourra utiliser pour s’abonner aux flux. L’action de ce signale est un ajout d’un membre, « sip ».


### 3. Restriction et isolation

Il est possible de lancer des services temporaires, principe de sandbox avec `systemd-run`. Ici je fais l’essai avec sshd qui prend un nom un peu complex d’où les erreurs de frappe… Si l’on vérifie l’état du service on constate qu’il existe bel et bien mais qu’il ne démarre pas car le vrai service sshd est déjà bind au port 22 :

 ![22](TheForbiddenFolder/22.png)
 
Une fois créer on peut retrouver les services temporaires dans `/run/systemd/transient/`. Dans la liste des services avec systemctl et dans les cgroups si un processus tourne, soit que le service soit actif, avec `systemd-cgls` :

 ![23](TheForbiddenFolder/23.png)
 
Le service appartient au cgroup system.slice.

Il est également possible de passer des paramètres spécifiques aux process directement au démarrage avec : `systemd-run -p MemoryMax=512M \<PROCESS>`

Cela peut permettre de tester de nouveaux arguments ou tout simplement de lancer un process très gourmand avec une limitation de RAM histoire de pouvoir faire d'autres choses à côté.


## IV. systemd units in-depth
### 1. Exploration de services existants

 ![24](TheForbiddenFolder/24.png)
 
La clef `ExecStartPost` permet d’executer une commande après `ExecStart`, d’où le « post ».

 ![25](TheForbiddenFolder/25.png)
 
`MemoryDenyWriteExecute` permet d’empêcher aux services de changer de code dynamiquement


### 2. Création de service simple

Voici un service maison contenant une description et qui permet de lancer nginx et d’ouvrir le port 80 ainsi que de le fermer lors de l’arrêt du service.

 ![26](TheForbiddenFolder/26.png)
 
Vérification du bon fonctionnement du service :

 ![27](TheForbiddenFolder/27.png)
 
Vérification de la création et suppression de la règle iptables :

 ![28](TheForbiddenFolder/28.png)
 
Afin de pouvoir démarrer le service au boot de la machine, il faut rajouter la section suivante :

 ![29](TheForbiddenFolder/29.png)
 
Cela permettra de cibler le processus via la cible `multi-user.target`, qui au boot de la machine va démarrer tous les services étant `WantedBy`.

On peut donc créer notre symlink :

 ![30](TheForbiddenFolder/30.png)
 
 
### 3. Sandboxing

`Systemd-analyze security <SERVICE>` permet de faire une analyse de sécurité d’un service. Pratique pour savoir si notre service est viable ou non :

 ![31](TheForbiddenFolder/31.png)
 
Comme on peut le constater un service basique c’est le top niveau insécurité, beau score.


Afin d’améliorer la sécurité de notre service je lui rajoute les options suivantes :

ProtectHostname : permet de protéger d’un changement de nom d’hôte ou de nom de domaine
RestrictNamespaces : permet d’empêcher notre service de créer de nouveaux namespaces
ProtectSystem : permet d’empêcher notre service de modifier des valeurs côté kernel

Avec ces quelques mesures on obtient déjà un score un peu plus bas :

 ![32](TheForbiddenFolder/32.png)